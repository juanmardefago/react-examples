import React from 'react';
import Button from './Button';
import Counter from './Counter';

export default class Container extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 0
    };
  }

  onClickHandler = () => {
    let { clicks } = this.state;
    clicks += 1;
    this.setState({ clicks });
  }

  render() {
    const { onClickHandler } = this;
    const { clicks } = this.state;

    return (
      <div>
        Class based implementation
        <Button onClick={onClickHandler} />
        <Counter clicks={clicks} />
      </div>
    );
  }
}
