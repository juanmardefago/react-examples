import React from 'react';
import PropTypes from 'prop-types';
import './Button.css'

function Button({ onClick, buttonText }) {
  return (
    <div className="app-button__container">
      <button className="app-button" onClick={onClick}>
        { buttonText }
      </button>
    </div>
  );
}

Button.propTypes = {
  onClick: PropTypes.func,
  buttonText: PropTypes.string
}

Button.defaultProps = {
  onClick: () => {},
  buttonText: 'Click me!'
}

export default Button;
