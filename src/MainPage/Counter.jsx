import React from 'react';
import PropTypes from 'prop-types';
import './Counter.css';

function Counter({ clicks }) {
  return (
    <div className="app-counter">
      Times clicked: {clicks}
    </div>
  );
};

Counter.propTypes = {
  clicks: PropTypes.number
};

Counter.defaultProps = {
  clicks: 0
};

export default Counter;
