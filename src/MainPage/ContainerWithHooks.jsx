import React, { useState } from 'react';
import Button from './Button';
import Counter from './Counter';

export default function ContainerWithHooks() {
  const [counter, setCounter] = useState(0);

  function onClickHandler() {
    setCounter(counter + 1);
  };

  return (
    <div>
      Hooks based implementation
      <Button onClick={onClickHandler} />
      <Counter clicks={counter} />
    </div>
  );
}
