import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import ContainerWithHooks from './MainPage/ContainerWithHooks';
import Container from './MainPage/Container';
import Button from './MainPage/Button';

function App() {
  const [withHooks, setWithHooks] = useState(false);

  function toggleImplementation() {
    setWithHooks(!withHooks);
  };

  return (
    <div className="App">
      <div className="App-content">
        <Button
        onClick={toggleImplementation}
        buttonText="Switch implementation"
        />
        <img src={logo} alt="logo" className="App-logo" />
        { withHooks? (
            <ContainerWithHooks />
          ) : (
            <Container />
          )
        }
      </div>
    </div>
  );
}

export default App;
